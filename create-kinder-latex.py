import base64
from io import BytesIO
import json
from PIL import Image
from pathlib import Path

PICTURE_DIRECTORY = Path('CHANGME')
PICTURE_ROTATION_ANGLE = 0
PICTURE_DUMMY = 'dummy.png'
CHILDREN_DATA_FILE = Path('CHANGME')
OUTPUT_FILE = Path('CHANGME')
IMAGE_FORMAT = "jpg"
LATEX_HEADER = r"""\documentclass[landscape, a5paper, 12pt]{scrartcl}

\usepackage[ngerman]{babel} %Sprache
\usepackage[utf8]{inputenc} %Zeichenkodierung
\usepackage[T1]{fontenc}
\usepackage{lmodern}
\usepackage{float}
\usepackage[table]{xcolor}
\usepackage{graphicx}
\usepackage{amsmath, amssymb}
\usepackage[top=1cm, bottom=1cm, left=1cm, right=1cm]{geometry}
\renewcommand{\arraystretch}{1.2}
\pagestyle{empty}

% Checkboxes
\newcommand{\checkedBox}{\textcolor{teal}{\mbox{\ooalign{$\checkmark$\cr\hidewidth$\square$\hidewidth\cr}}}}
\newcommand{\uncheckedBox}{\textcolor{red}{\ensuremath{\boxtimes}}}


\begin{document}
"""
LATEX_FOOTER = r"\end{document}"

def save_image(base64_image_string: str, image_filename: str) -> None:
    image_data = base64.b64decode(base64_image_string.replace("data:image/png;base64,",""))
    image = Image.open(BytesIO(image_data))
    image.save(PICTURE_DIRECTORY / image_filename, IMAGE_EXTENSION)

def get_image_filename(familyName: str, callName: str) -> str:
    special_char_map = {ord('ä'): 'ae', ord('ü'): 'ue',
                        ord('ö'): 'oe', ord('ß'): 'ss'}
    image_filename =  (
        (familyName + callName)
        .translate(special_char_map)
        .replace(" ", "")
        .lower()
    ) + "." + IMAGE_EXTENSION
    return image_filename


def checkbox(value: str) -> str:
    if value:
        return r"\checkedBox"
    else:
        return r"\uncheckedBox"

def tex_contacts(contacts: list[dict[str,str]]) -> str:
    tex_code = ""
    for i, contact in enumerate(contacts):
        tex_code += rf"""Notfallname {i+1}: & {contact.get('name', '')} \\
    Notfallnummer {i+1}: & {contact.get('telephone', '')} \\
    """
    return tex_code

def write_participant(data: dict[str, str | dict[str,str] | list[str] | list[dict[str,str]]]) -> str:

    familyName = data.get("participant", {}).get("familyName", "")
    callName = data.get("participant", {}).get("callName", "")
    if not familyName and not callName:
        return ""
    if base64_image_string := data.get("signupPhoto", ""):
        image_filename = get_image_filename(familyName, callName)
        save_image(base64_image_string, image_filename)
        tex_picture_code = fr"\includegraphics[width=0.9\textwidth, angle={PICTURE_ROTATION_ANGLE}]{{{PICTURE_DIRECTORY / image_filename}}}"
    else:
        tex_picture_code = fr"\includegraphics[width=0.9\textwidth]{{{PICTURE_DIRECTORY / PICTURE_DUMMY}}}"

    return rf"""  \begin{{minipage}}{{0.35\textwidth}}
    {tex_picture_code}
  \end{{minipage}}
  \begin{{minipage}}{{0.6\textwidth}}
    \rowcolors{{1}}{{gray!20}}{{white}}
    \begin{{tabular}}{{@{{}}p{{0.35\textwidth}} p{{0.65\textwidth}}@{{}}}}
    Vorname: & {callName} \\
    Nachname: & {familyName} \\
    Geschlecht: & {data.get("participant", {}).get("gender", "")} \\
    Zirkel: & {(data.get("zirkel", {}) or {}).get("name", "") or ""} \\
    Zimmer: & {data.get("roomNumber", "") or ""} \\
    Unverträglichkeiten: & {data.get("foodRestriction", "") or ""} \\
    Medizinische Infos: & {", ".join(data.get("medicalNotes", []))} \\
    {tex_contacts(data.get("contacts", []))}
    Sonstiges: & {data.get("notes", "") or ""} \\
    \multicolumn{{2}}{{c}}{{{checkbox(data.get("pool", False))}~Schwimmen~~{checkbox(data.get("leavingPremise", False))}~Ausgang~~{checkbox(data.get("removeTicks", False))}~Zecken}}
    \end{{tabular}}
  \end{{minipage}}

  \clearpage
"""


def write_full_file(content: str) -> str:
    return f"""{LATEX_HEADER}

{content}
{LATEX_FOOTER}
"""


def main():
    participants_content = ""
    with CHILDREN_DATA_FILE.open(mode='r', newline='', encoding='utf-8') as input_file:
        data = json.load(input_file)

    stripped_data = data.get("data", {}).get("event", {}).get("extensions", [])
    filtered_data = [
        d for d in stripped_data if d.get("confirmed", False) and not d.get("timeOfSignoff", None)
    ]

    for participant in filtered_data:
        participants_content = participants_content + write_participant(participant) + "\n\n"

    latex_content = write_full_file(participants_content)

    with OUTPUT_FILE.open(mode='w', encoding='utf-8') as output_file:
        output_file.write(latex_content)


if __name__ == "__main__":
    main()
